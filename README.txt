HySS Contact Form MyAdmin
=========================

Formulir kontak dengan HySS dan MySQL dilengkapi dengan halaman dashboard admin
untuk memonitor dan mengontrol pesan yang masuk.

Halaman formulir kontak dapat dipergunakan oleh pihak customer untuk mengirim
pesan kepada perusahaan. Halaman dashboard admin dilengkapi dengan sistem login
yang dapat digunakan oleh perusahaan untuk memonitor dan mengontrol pesan yang
masuk.

Formulir kontak dilengkapi dengan mail function dan menyimpan data di database.


Modul
-----
Terdiri dari dua modul : 1) Modul user, dan 2) Modul admin.

 
Software dan OS
---------------
   * HySS 1.0.1
   * cLHy 1.6.26
   * MySQL 8.0.20
   * OS Ubuntu 20.04 LTS


Cara menjalankan
----------------
   * Clone kode sumber :
	$ git clone https://habibinrhmn@bitbucket.org/habibinrhmn/hyss-contact-myadmin.git

   * Salin direktori repositori kode sumber ke document root cLHy, misalnya :
	$ sudo cp -r hyss-contact-myadmin /usr/local/clhydelman/htdocs/

   * Melalui MySQL, buat database dengan nama contactdb

   * Import file basis data contactdb.sql yang ada di folder sql_file

   * Ganti password root MySQL yang ada di file config.hyss dan admin/includes/config.hyss

   * Jalankan skrip di browser web : http://localhost/hyss-contact-myadmin

   * Untuk login ke halaman dashboard admin : http://localhost/hyss-contact-myadmin/admin
     
	Username : admin
        Password : Test@123

